# Product 1:

## Product Name:
DRS -  Decision Review System 
It is one of the most important tool in many sports like -
1. Cricket
1. Tennis
1. Volleyball

## Product Link:
[Link](https://www.analyticsvidhya.com/blog/2020/03/ball-tracking-cricket-computer-vision/)

## Product Short Description:

DRS mainly uses the tool called 'Ball Tracking', in which the ball is tracked throughout its path, and predicts whether the ball hits the stupms or not.
There are mainly there steps involved during this process of decision making i.e - 
1. Detecting the ball
1. Tracking of the ball
1. Predicting whether the ball will hit the stupms or not

## Product is combination of features:

* Object detection
* computer vision
* Object Tracking
* Predicting the outcome (considering variouis features)

## Orgainzations/Companies using the product:
1. ICC
1. IPL

---
---

# Product 2:

## Product Name:
Animoji

## Product Link:
[Link](https://www.pocket-lint.com/apps/news/apple/142230-what-are-animoji-how-to-use-apple-s-animated-emoji)

## Product Short Description:

Apple's Animoji are custom, animated versions of popular emoji characters. These are custom animated messages that use your voice and reflect your facial expressions.
This involves few steps like - 
1. It recognises the face
1. Then it enables FaceID - which enables to detect facial expressions 
1. Sound detection

## Product is combination of features:

* Face recognition

## Orgainzations/Companies using the product:
1. Apple

---
---


# Product 3:

## Product Name:

Autopilot

## Product Link:
[Link](https://www.tesla.com/autopilot)

## Product Short Description:
In this, there is camera attached with a car, which provides the vision of outside world. Through the concept of Object dtection and recognition it identifies the various trained Objects and accordingly takes action to it.


## Product is combination of features:

* Object detection
* computer vision
* Object recognition

## Orgainzations/Companies using the product:
Tesla
